module MinHS.TyInfer where

import qualified MinHS.Env as E
import MinHS.Syntax
import MinHS.Subst
import MinHS.TCMonad

import Data.Monoid (Monoid (..), (<>))
import Data.Foldable (foldMap)
import Data.List (nub, union, (\\))

primOpType :: Op -> QType
primOpType Gt   = Ty $ Base Int `Arrow` (Base Int `Arrow` Base Bool)
primOpType Ge   = Ty $ Base Int `Arrow` (Base Int `Arrow` Base Bool)
primOpType Lt   = Ty $ Base Int `Arrow` (Base Int `Arrow` Base Bool)
primOpType Le   = Ty $ Base Int `Arrow` (Base Int `Arrow` Base Bool)
primOpType Eq   = Ty $ Base Int `Arrow` (Base Int `Arrow` Base Bool)
primOpType Ne   = Ty $ Base Int `Arrow` (Base Int `Arrow` Base Bool)
primOpType Neg  = Ty $ Base Int `Arrow` Base Int
primOpType Fst  = Forall "a" $ Forall "b" $ Ty $ (TypeVar "a" `Prod` TypeVar "b") `Arrow` TypeVar "a"
primOpType Snd  = Forall "a" $ Forall "b" $ Ty $ (TypeVar "a" `Prod` TypeVar "b") `Arrow` TypeVar "b"
primOpType _    = Ty $ Base Int `Arrow` (Base Int `Arrow` Base Int)

constType :: Id -> Maybe QType
constType "True"  = Just $ Ty $ Base Bool
constType "False" = Just $ Ty $ Base Bool
constType "()"    = Just $ Ty $ Base Unit
constType "Pair"  = Just
                  $ Forall "a"
                  $ Forall "b"
                  $ Ty
                  $ TypeVar "a" `Arrow` (TypeVar "b" `Arrow` (TypeVar "a" `Prod` TypeVar "b"))
constType "Inl"   = Just
                  $ Forall "a"
                  $ Forall "b"
                  $ Ty
                  $ TypeVar "a" `Arrow` (TypeVar "a" `Sum` TypeVar "b")
constType "Inr"   = Just
                  $ Forall "a"
                  $ Forall "b"
                  $ Ty
                  $ TypeVar "b" `Arrow` (TypeVar "a" `Sum` TypeVar "b")
constType _       = Nothing

type Gamma = E.Env QType

initialGamma :: Gamma
initialGamma = E.empty

tv :: Type -> [Id]
tv = tv'
 where
   tv' (TypeVar x) = [x]
   tv' (Prod  a b) = tv a `union` tv b
   tv' (Sum   a b) = tv a `union` tv b
   tv' (Arrow a b) = tv a `union` tv b
   tv' (Base c   ) = []

tvQ :: QType -> [Id]
tvQ (Forall x t) = filter (/= x) $ tvQ t
tvQ (Ty t) = tv t

tvGamma :: Gamma -> [Id]
tvGamma = nub . foldMap tvQ

infer :: Program -> Either TypeError Program
infer program = do (p',tau, s) <- runTC $ inferProgram initialGamma program
                   return p'

unquantify :: QType -> TC Type
unquantify = unquantify' 0 emptySubst
unquantify' :: Int -> Subst -> QType -> TC Type
unquantify' i s (Ty t) = return $ substitute s t
unquantify' i s (Forall x t) = do x' <- fresh
                                  unquantify' (i + 1)
                                              ((show i =: x') <> s)
                                              (substQType (x =:TypeVar (show i)) t)

unify :: Type -> Type -> TC Subst
unify (TypeVar a) (TypeVar b) | a == b = return( emptySubst )
                              | otherwise = return( a =: TypeVar b )
unify t1@(Base bt1) t2@(Base bt2) =
    case bt1 == bt2 of
        True  -> return( mempty )
        False -> error "found it 1"
unify (Prod t11 t12) (Prod t21 t22) = do
    s  <- unify t11 t21
    s' <- unify (substitute s t12) (substitute s t22)
    return( s<>s' )
unify (Sum t11 t12) (Sum t21 t22) = do
    s  <- unify t11 t21
    s' <- unify (substitute s t12) (substitute s t22)
    return( s<>s' )
unify (Arrow t11 t12) (Arrow t21 t22) = do
    s  <- unify t11 t21
    s' <- unify (substitute s t12) (substitute s t22)
    return( s<>s' )
unify (TypeVar v) t =
    case elem v (tv t) of
        True  -> error "found it 2" 
        False -> return( v =: t )
unify t (TypeVar v) =
    case elem v (tv t) of
        True -> error "found it 3"
        False -> return ( v =: t )
unify (Base bt1) (Arrow a b) = 
    case b of
        (Base b2) -> error "mismatched"
        (TypeVar v2) -> error v2
        (Arrow a b) -> case a of
            TypeVar b3 -> error b3
unify t1 t2 = error "fail"

generalise :: Gamma -> Type -> QType
generalise g t = 
    generalise' ((tv t) \\ (tvGamma g)) t
    where
        generalise' [] t = Ty t
        generalise' (s:sx) t = Forall s (generalise' sx t)

inferProgram :: Gamma -> Program -> TC (Program, Type, Subst)
inferProgram g [Bind i Nothing [] exp] = do
    (e, t, s) <- inferExp g exp
    return ( [Bind i (Just (generalise g t)) [] (allTypes (substQType s) e)], t, s)

inferExp :: Gamma -> Exp -> TC (Exp, Type, Subst)

inferExp g e@(Num n) = return( e, Base Int, emptySubst )

inferExp g e@(Con c) = do
    t <- unquantify(t')
    return( e, t, emptySubst )
    where Just t' = constType c

inferExp g e@(Prim o) = do
    t <- unquantify(primOpType o)
    return ( e, t, emptySubst )

inferExp g e@(Var i) =
    case E.lookup g i of
        Just qt -> do
            unq <- unquantify qt
            return (Var i, unq, emptySubst)

inferExp g e@(App e1 e2) = do
    (e1', t1', s1') <- inferExp g e1
    (e2', t2', s2') <- inferExp (substGamma s1' g) e2
    a               <- fresh
    u               <- unify (substitute s2' t1') (Arrow t2' a)
    return( (App e1' e2'), substitute u a, u<>s1'<>s2' )


inferExp g e@(If c t el) = do
    (e1, t1, s1)   <- inferExp g c
    u              <- unify t1 (Base Bool)
    (e2, t2, s2)   <- inferExp (substGamma (s1<>u) g) t
    (e3, t3, s3)   <- inferExp (substGamma (s1<>u<>s2) g) el
    u'             <- unify ( substitute s3 t2 ) t3 
    return( (If e1 e2 e3), substitute u' t3, (u<>u'<>s1<>s2<>s3) )

inferExp g e@(Case exp [Alt "Inl" [i1] e1, Alt "Inr" [i2] e2]) = do
    (e', t', s')    <- inferExp g exp
    a1              <- fresh
    (e1', t1', s1') <- inferExp (substGamma s' (E.add g (i1, Ty a1))) e1
    a2              <- fresh
    (e2', t2', s2') <- inferExp (substGamma (s'<>s1') (E.add g (i2, Ty a2))) e2
    u               <- unify (substitute (s'<>s1'<>s2') (Sum a1 a2) ) (substitute (s1'<>s2') t')
    u'              <- unify (substitute (u<>s2') t1') (substitute u t2')
    return( Case e' [Alt "Inl" [i1] e1, Alt "Inr" [i2] e2], substitute (u'<>u) t2', (u<>u'<>s'<>s1'<>s2'))

inferExp g (Case e _) = typeError MalformedAlternatives

inferExp g expr@(Letfun (Bind f t [x] e) ) = do
    a1            <- fresh
    a2            <- fresh
    (e', t', s')  <- inferExp (E.addAll g [(x, (Ty a1)), (f, (Ty a2))]) e
    u             <- unify (substitute s' a2) (Arrow (substitute s' a1) t')
    return( allTypes (substQType (u<>s')) ( Letfun (Bind f (Just (Ty (substitute (u) (Arrow (substitute s' a1) t') ) ) ) [x] e') ),
            substitute u ( Arrow (substitute s' a1) t' ),
            u<>s')

inferExp g e@(Let [Bind i t a e1] e2) = do
    (e1', t1', s1') <- inferExp g e1
    (e2', t2', s2') <- inferExp (E.add (substGamma s1' g) (i, generalise (substGamma s1' g) t1' )) e2
    return( allTypes (substQType (s1'<>s2')) (Let [Bind i (Just (generalise (substGamma s1' g) t1')) a e1'] e2') ,
            t2',
            (s1'<>s2'))